//
//  main.m
//  ChurchProto
//
//  Created by Ayorinde Adesugba on 6/13/12.
//  Copyright (c) 2012 Ayorinde Adesugba. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "com_siscAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([com_siscAppDelegate class]));
    }
}
