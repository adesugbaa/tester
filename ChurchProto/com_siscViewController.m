//
//  com_siscViewController.m
//  ChurchProto
//
//  Created by Ayorinde Adesugba on 6/13/12.
//  Copyright (c) 2012 Ayorinde Adesugba. All rights reserved.
//

#import "com_siscViewController.h"

@interface com_siscViewController ()

@end

@implementation com_siscViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}

@end
