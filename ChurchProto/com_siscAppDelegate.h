//
//  com_siscAppDelegate.h
//  ChurchProto
//
//  Created by Ayorinde Adesugba on 6/13/12.
//  Copyright (c) 2012 Ayorinde Adesugba. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface com_siscAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
